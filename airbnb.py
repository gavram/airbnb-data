import lxml.html
from os import path
import urllib

# prepare folder to save downloaded files
if not os.path.exists('data'):
    os.makedirs('data')
    
# Function for parsing url to get folder and file name
def parse_url(url):
    global folder_name
    global file_name

    if 'data' in url:
        folder_name = 'data'
        if 'listings.csv.gz' in url:
            file_name = 'listings.csv.gz'
        elif 'calendar.csv.gz' in url:
            file_name = 'calendar.csv.gz'
        else:
            file_name = 'reviews.csv.gz'
    
    if 'visualisations' in url:
        folder_name = 'visualisations'
        if 'neighbourhoods.csv' in url:
            file_name = 'neighbourhoods.csv'
        elif 'neighbourhoods.geojson' in url:
            file_name = 'neighbourhoods.geojson'
        elif 'listings.csv' in url:
            file_name = 'listings.csv'
        else:
            file_name = 'reviews.csv'
        
    return folder_name;
    return file_name;

# Function for parsing date from "dd MMM, yyyy" to "yyyy-mm-dd"
def parse_date(date):
    global date_new
    if date != 'N/A': 
        dict_month = {'January': '01',
                      'February': '02',
                      'March': '03',
                      'April': '04',
                      'May': '05',
                      'June': '06',
                      'July': '07',
                      'August': '08',
                      'September': '09',
                      'October': '10',
                      'November': '11',
                      'December': '12'}
    
        lhs, year = date.split(', ', 1)
        day, month = lhs.split(' ', 1)
        month = dict_month[month]
        date_new = year + '-' + month + '-' + day
        return date_new;
    
# Main Script            
# Retreive page content
tree = lxml.html.parse('http://insideairbnb.com/get-the-data.html')

# Extract data from page content
rows = tree.xpath('//table/tbody/tr')

i = 1
for row in rows:
    # city name
    city = row[1].text
    
    # date in format "dd MMM, yyyy"
    date = row[0].text
    
    # date in format "yyyy-mm-dd"
    parse_date(date)
    
    # get file download url
    url = row[2].xpath('a/@href')[0]
    
    # parse url to obtain folder path and file name
    parse_url(url)
    
    # creating folders structure if needed
    if not os.path.exists('data/' + city):
        os.makedirs('data/' + city)
    if date != 'N/A':
        if not os.path.exists('data/' + city + '/' + date_new):
            os.makedirs('data/' + city + '/' + date_new)
            os.makedirs('data/' + city + '/' + date_new + '/' + 'data')
            os.makedirs('data/' + city + '/' + date_new + '/' + 'visualisations')
    
    # download and save files in proper folder
    if not os.path.exists('data/' + city + '/' + date_new + '/' + folder_name + '/' + file_name):
        file_path = 'data' + '/' + city + '/' + date_new + '/' + folder_name + '/' + file_name
        urllib.request.urlretrieve(url, file_path)
    
    print(city + ' ' + date_new + ' ' + 'downloading file ' + file_name + ' - ' + str(i+1) + ' of ' + str(len(rows)), end = '\r')
    i = i + 1