# AirBnB data

## AirBnB data from 2015


### Definition:

This dataset contains all data for over 100 cities since 2015. All thanks go to Tom Slee who did a great job with https://github.com/tomslee/airbnb-data-collection. The data archive is located at http://insideairbnb.com/get-the-data.html and this project is for downloading data from this site and updating new data.


## Data

Data obtained from Inside AirBnB http://insideairbnb.com/get-the-data.html
The data behind the Inside Airbnb site is sourced from publicly available information from the Airbnb site.
The data has been analyzed, cleansed and aggregated where appropriate to faciliate public discussion


## Explanations

Files are downloaded using airbnb.py script from the aforementioned web address. Script downloads files and save them in given folder structure.
There are over 13K files for over 100 cities. Script downloads only new files that are not already downloaded and stored on local computer.
The entire set of files has a size that exceeds 100GB of data, and the number of files is over 13K.

## Usage

* Just clone or copy this repo on your local computer.
* Change path to folder where airbnb.py is located.
* In terminal type: python airbnb.py
* There are no requirements to run this script. Only standard Python libraries were used.


## Files

When you run this script the files will be downloaded to a folders with the following structure

    data--city_name_1
        |        |--date_1
        |        |      |--data        
        |        |      |     |--listings.csv.gz      
        |        |      |     |--calendar.csv.gz     
        |        |      |     |--reviews.csv.gz            
        |        |      |                    
        |        |      |--visualisations                 
        |        |           |--listings.csv           
        |        |           |--reviews.csv           
        |        |           |--neighbourhoods.csv           
        |        |           |--neighbourhoods.geojson
        |        |      
        |        |--date_2....
        |
        |-city_name_2.....
        
* listings.csv.gz           - Detailed Listings data
* calendar.csv.gz           - Detailed Calendar Data for listings
* reviews.csv.gz            - Detailed Review Data for listings
* listings.csv              - Summary information and metrics for listings (good for visualisations)
* reviews.csv               - Summary Review data and Listing ID (to facilitate time based analytics and visualisations linked to a listing)
* neighbourhoods.csv        - Neighbourhood list for geo filter. Sourced from city or open source GIS files
* neighbourhoods.geojson    - GeoJSON file of neighbourhoods of the city
* airbnb.py                 - Python script that obtain and process whole dataset
* datapackage.json          - datapackage for sample file data/Athens/2019-09-20/visualisations/listings.csv
* README.md                 - this readme file




## License

The usage of this dataset is covered http://insideairbnb.com/about.html#disclaimers and if you would like to do further analysis or produce alternate visualisations of the data, it is available below under a Creative Commons CC0 1.0 Universal (CC0 1.0) "Public Domain Dedication" license.
Python scripts are free to use and distribute

